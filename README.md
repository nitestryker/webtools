webtools.class.php
==================
a php class with useful tools

### Usage Examples:

###### Connect to MySQL 
 
 example:
 
Open($database, $server, $username, $password, $charset, $pcon = true or false)

setting the db params
```PHP
  $db = new webtools();
  if (! $db->Open("database", "db_server", "db_username", "db_password")) { 
    $db->Kill(); 
} 
 // successful connection do this


```
or you can use the pre-set params
```PHP
$db = new webtools();
 if (! $db->Open()) { 
    $db->Kill(); 
}
 // successful connection  do this 
 

```
######  IP to Location 
 
info params ( statusCode, statusMessage, ipAddress, countryCode, countryName,regionName , cityName , zipCode , latitude , longitude, timeZone )  

```PHP
$ip = webtools::ipLocation('ip_address', 'info_param');

```


###### Create a Salted md5 password 
```PHP
$password_string = "password_here"; 
$salted_md5 = webtools::SaltedMd5($password_string)

```
###### Validate a Salted md5 password (boolean)

```PHP
 $check = webtools::validateSaltedMd5($salted_md5, $password_string);
```

###### Shorten URL with Bitly API
```PHP
$get = new webtools();
$results = $get->bitlyShorten('http:/www.website.com','bitly_username','Bitly_API_key');
echo $results;
```
###### Email validation

```PHP
$check = new webtools();
if ($check->validEmail($email))
{
      /* if email is vaild  do this */
   
}
else
{
      /* if email is not valid do this */
    
}
```
###### Sanatize User's Input

```PHP
$new = new webtools();
$clean_data = $new->clean($data);
echo $clean_data;
```
