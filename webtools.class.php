<?php
/*
+------------------------------------------------
|    WebTools
|   =============================================
|    by nitestryker
|   (c) 2013 nswebdev.com
|   http://www.nswebdev.com
|   =============================================
|   git: https://github.com/nsdev/webtools.class.php.git 
|   Licence Info: GPL
+------------------------------------------------
*/
class webtools
{
 // Set Mysql Credentials
	private $db_host    = "localhost";  // server name
	private $db_user    = "";           // user name
	private $db_pass    = "";           // password
	private $db_dbname  = "";           // database name
	private $db_charset = "";           // optional character set (i.e. utf8)
	private $db_pcon    = false;        // use persistent connection?
  private $active_row     = -1;       // current row
	private $error_desc     = "";       // last mysql error string
  public $ThrowExceptions = false;    // 





 /* 
   constructor 
 */
 public function __construct($connect = true, $database = null, $server = null,
								$username = null, $password = null, $charset = null) {

		if ($database !== null) $this->db_dbname  = $database;
		if ($server   !== null) $this->db_host    = $server;
		if ($username !== null) $this->db_user    = $username;
		if ($password !== null) $this->db_pass    = $password;
		if ($charset  !== null) $this->db_charset = $charset;

		if (strlen($this->db_host) > 0 &&
			strlen($this->db_user) > 0) {
			if ($connect) $this->Open();
		}
	}
 
  /*
   Open Connection to Database
  */
  public function Open($database = null, $server = null, $username = null,
						 $password = null, $charset = null, $pcon = false) {
		$this->ResetError();

		// use pre-set values 
		if ($database !== null) $this->db_dbname  = $database;
		if ($server   !== null) $this->db_host    = $server;
		if ($username !== null) $this->db_user    = $username;
		if ($password !== null) $this->db_pass    = $password;
		if ($charset  !== null) $this->db_charset = $charset;
		if (is_bool($pcon))     $this->db_pcon    = $pcon;

		$this->active_row = -1;

		// Open persistent or normal connection
		if ($pcon) {
			$this->mysql_link = @mysql_pconnect(
				$this->db_host, $this->db_user, $this->db_pass);
		} else {
			$this->mysql_link = @mysql_connect (
				$this->db_host, $this->db_user, $this->db_pass);
		}
		// Connect to mysql server failed?
		if (! $this->IsConnected()) {
			$this->SetError();
			return false;
		} else {
			// Select a database (if specified)
			if (strlen($this->db_dbname) > 0) {
				if (strlen($this->db_charset) == 0) {
					if (! $this->SelectDatabase($this->db_dbname)) {
						return false;
					} else {
						return true;
					}
				} else {
					if (! $this->SelectDatabase(
						$this->db_dbname, $this->db_charset)) {
						return false;
					} else {
						return true;
					}
				}
			} else {
				return true;
			}
		}
	}
  
  //  Selects database & charset 
   public function SelectDatabase($database, $charset = "") {
		$return_value = true;
		if (! $charset) $charset = $this->db_charset;
		$this->ResetError();
		if (! (mysql_select_db($database))) {
			$this->SetError();
			$return_value = false;
		} else {
			if ((strlen($charset) > 0)) {
				if (! (mysql_query("SET CHARACTER SET '{$charset}'", $this->mysql_link))) {
					$this->SetError();
					$return_value = false;
				}
			}
		}
		return $return_value;
	}
   // db is connected 
  public function IsConnected() {
		if (gettype($this->mysql_link) == "resource") {
			return true;
		} else {
			return false;
		}
	}
  
  // Kill Message  
  public function Kill($message = "") {
		if (strlen($message) > 0) {
			exit($message);
		} else {
			exit($this->Error());
		}
	}
  
  // on error display error message + number 
  public function Error() {
		$error = $this->error_desc;
		if (empty($error)) {
			if ($this->error_number <> 0) {
				$error = "Unknown Error (#" . $this->error_number . ")";
			} else {
				$error = false;
			}
		} else {
			if ($this->error_number > 0) {
				$error .= " (#" . $this->error_number . ")";
			}
		}
		return $error;
	}
  
  // set error message & number 
  private function SetError($errorMessage = "", $errorNumber = 0) {
		try {
			if (strlen($errorMessage) > 0) {
				$this->error_desc = $errorMessage;
			} else {
				if ($this->IsConnected()) {
					$this->error_desc = mysql_error($this->mysql_link);
				} else {
					$this->error_desc = mysql_error();
				}
			}
			if ($errorNumber <> 0) {
				$this->error_number = $errorNumber;
			} else {
				if ($this->IsConnected()) {
					$this->error_number = @mysql_errno($this->mysql_link);
				} else {
					$this->error_number = @mysql_errno();
				}
			}
		} catch(Exception $e) {
			$this->error_desc = $e->getMessage();
			$this->error_number = -999;
		}
		if ($this->ThrowExceptions) {
			if (isset($this->error_desc) && $this->error_desc != NULL) {
				throw new Exception($this->error_desc . ' (' . __LINE__ . ')');
			}
		}
	}
  
   /**
	 * Resets the error data 
	 *
	 */
	private function ResetError() {
		$this->error_desc = '';
		$this->error_number = 0;
	}
  
/*
 Ip Location info 
*/
public static function ipLocation($ip,$info="")
{
$url ="http://api.ipinfodb.com/v3/ip-city/?key=6d6b44b869b0bd0b965f848b8a2838c795943be7f58cdde4e767149c81886563&ip=$ip&format=json";   
$s = curl_init();   
curl_setopt($s,CURLOPT_URL, $url);   
curl_setopt($s,CURLOPT_HEADER,false);   
curl_setopt($s,CURLOPT_RETURNTRANSFER,1);   
$result = curl_exec($s);   
curl_close( $s );   
$obj = json_decode($result, true);

//  info selection 
switch ($info) {
  case "statusCode":
        return $obj['statusCode'];
        break;
  case "statusMessage":
        return $obj['statusMessage'];
        break;
  case "ipAddress":
        return $obj["ipAddress"];
        break;
  case "countryCode":
        return $obj["countryCode"]; 
        break;
  case "countryName":
        return $obj['countryName'];
        break;
  case "regionName":
        return $obj['regionName'];
        break;
  case "cityName":
        return $obj['cityName'];
        break;
  case "zipCode":
       return $obj['zipCode'];
       break;
  case "latitude":
        return $obj['latitude'];
        break;
  case "longitude": 
        return $obj['longitude'];   
        break;
 case "timeZone":
        return $obj['timeZone'];
        break;                            
}
}


 /*
  Salt for md5 
*/
const salt = "PrUcha6uwUF4tRusTuwr9wuKuYestUf9wRuc78c2dUVuf3av6PEWE95a86fRU5hu"; 


/*
 Create a Salted md5 password 
*/
public static function SaltedMd5($string) 
    { 
        return md5($string . self::salt); 
    } 
    
/*
    Validate Salted Md5 Password
*/
public static function validateSaltedMd5($saltedMd5, $givenString) 
    { 
        return ( $saltedMd5 === self::SaltedMd5($givenString) ); 
    } 

/*
   sanitize user input 
*/
function clean($value) {

       // If magic quotes not turned on add slashes.
       if(!get_magic_quotes_gpc())

       // Adds the slashes.
       { $value = addslashes($value); }

       // Strip any tags from the value.
       $value = strip_tags($value);

       //  protect against sql injection 
       $value = mysql_real_escape_string($value);
       // Return the value out of the function.
       return $value;
  }
   

/*
check if an email is vaild first by syntax then by host
*/
function validEmail($email)
{
       //Perform a basic syntax-Check
       //If this check fails, there's no need to continue
       if(!filter_var($email, FILTER_VALIDATE_EMAIL))
       {
               return false;
       }
 
       //extract host
       list($user, $host) = explode("@", $email);
       //check, if host is accessible
       if (!checkdnsrr($host, "MX") && !checkdnsrr($host, "A"))
       {
               return false;
       }
 
       return true;
}




/* shorten links using the bitl.ly api 
*/ 
function bitlyShorten($url,$login,$appkey,$format='txt') 
  {
	$connectURL = 'http://api.bit.ly/v3/shorten?login='.$login.'&apiKey='.$appkey.'&uri='.urlencode($url).'&format='.$format;
	$results = $this->curl_get_result($connectURL);
  return $results;
  }
  
  
  
 /*
   returns a result from url 
 */
function curl_get_result($url) 
  {
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
  }
  
  
  } /* end of class */
?>      
